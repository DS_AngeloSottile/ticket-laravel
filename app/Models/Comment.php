<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    protected $fillable =
    [
        'content',
        'post_id',
        'status',
    ];


    public function ticket()
    {
        return $this->belongsTo('App\Models\Ticket');
    }

    public function setContentAttribute($value)
    {
        return $this->attributes['content'] = $value.' aggiungo altro'; //strtoupper($value)
    }
}
