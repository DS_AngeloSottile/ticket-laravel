<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Ticket extends Model
{
    use Notifiable;
    protected $fillable =
    [
        'title',
        'content',
        'slug',
        'status',
    ];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment','post_id');
    }


    public function getRouteKeyName()
    {
        return 'slug';
    }
}
