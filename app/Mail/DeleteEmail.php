<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DeleteEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $title;
    protected $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title,$content)
    {
        $this->title    = $title;
        $this->content  = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.deleteMail',
        [
            'title'     => $this->title,
            'content'   => $this->content
        ]);
        //->attachData(public_path('storage/immagini/fcp.jpg'),'nome-file');
    }
}
