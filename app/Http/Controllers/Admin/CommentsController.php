<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentFormRequest;
use App\Models\Comment;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{

    public function newComment(CommentFormRequest $request,Ticket $ticket)
    {

        try
        {
            DB::beginTransaction();

                $commentData =
                [
                    'content' => $request->content,
                ];

                $comment = $ticket->comments()->create($commentData);

            DB::commit();

                session()->flash('success','commento creato con successo');
                return redirect()->back();

        }
        catch(\Exception $e)
        {

            $errors =
            [
                'error_message' => 'Non è stato possibile creare il commento'
            ];
            session()->flash('error',$errors);
            return redirect()->back();

        }

    }
}
