<?php

namespace App\Http\Controllers\Admin; //inseriti tu
use App\Http\Controllers\Controller; //inseriti tu


use App\Http\Requests\TicketsFormRequest;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::all();
        return view('ticket.index',compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ticket.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketsFormRequest $request)
    {

        try
        {
            DB::beginTransaction();

                $ticketData =
                [
                    'title' => $request->title,
                    'content' => $request->content,
                    'slug'   => uniqId(),
                ];

                //$insert_data = $request->except(['_token']); //entrambe valide
                $newTicket = Ticket::create($ticketData);

            DB::commit();

                session()->flash('success','tickets creato con successo L\'id è: '.$newTicket->slug);
                return redirect()->route('admin.tickets.index');
        }
        catch(\Exception $e)
        {
            $errors =
            [
                'error_message' => 'Non è stato possibile creare il ticket'
            ];
            session()->flash('error',$errors);
            return redirect()->route('admin.tickets.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        try
        {
            DB::beginTransaction();

                $ticket->load('comments');

            DB::commit();

                return view('ticket.show',compact('ticket'));
        }
        catch(\Exception $e)
        {

            $errors =
            [
                'error_message' => 'Ticket non trovato'
            ];

            session()->flash('error',$errors);
            return redirect()->route('admin.ticket.index');

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {

        if(!$ticket)
        {
            $errors =
            [
                'error_message' => 'Ticket non trovato'
            ];

            session()->flash('error',$errors);
            return redirect()->route('admin.ticket.index');
        }

        return view('ticket.edit',compact('ticket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketsFormRequest $request, Ticket $ticket)
    {

        try
        {
            DB::beginTransaction();

                if($request->status != null)
                {
                    $status = 0;
                }
                else
                {
                    $status = 1;
                }

                $updated = $ticket->update(
                [
                    'title'      =>     $request->title,
                    'content'    =>     $request->content,
                    'status'     =>     $status,
                ]);


            DB::commit();

                session()->flash('success',('Ticket mofificato con successo'));
                return redirect()->route('admin.tickets.index');
        }
        catch(\Exception $e)
        {

            $errors =
            [
                'error_message' => 'Errore nella modifica del ticket',
            ];

            session()->flash('error',$errors);
            return redirect()->route('admin.tickets.index');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {

        try
        {
            DB::beginTransaction();


                $deleted = $ticket->delete();

            DB::commit();

                session()->flash('success',('Ticket cancellato con successo'));
                return redirect()->route('admin.tickets.index');

        }
        catch(\Exception $e)
        {

            $errors =
            [
                'error_message' => 'Errore nella rimozione del ticket',
            ];

            session()->flash('error',$errors);
            return redirect()->route('admin.tickets.index');

        }

    }
}
