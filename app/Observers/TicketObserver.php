<?php

namespace App\Observers;

use App\Mail\DeleteEmail;
use App\Models\Ticket;
use App\Notifications\UpdateNotification;
use Illuminate\Support\Facades\Mail;

class TicketObserver
{
    /**
     * Handle the ticket "created" event.
     *
     * @param  \App\Ticket  $ticket
     * @return void
     */
    public function created(Ticket $ticket)
    {
        $data = array(
            'Ticket' =>$ticket->slug,
        );
        Mail::send('emails.ticket', $data, function ($message)
        {
            $message->from('info@tickets.com', 'Learning freeweb');
            $message->to('angelosottile4@hotmail.it');
            $message->subject('Hai ricevuto un nuovo ticket');
        });
    }

    /**
     * Handle the ticket "updated" event.
     *
     * @param  \App\Ticket  $ticket
     * @return void
     */
    public function updated(Ticket $ticket)
    {
        Mail::to('angelosottiled3@hotmail.it')->send(new DeleteEmail('Titolo custom ','contenuto custom'));
    }

    /**
     * Handle the ticket "deleted" event.
     *
     * @param  \App\Ticket  $ticket
     * @return void
     */
    public function deleted(Ticket $ticket)
    {

    }

    /**
     * Handle the ticket "restored" event.
     *
     * @param  \App\Ticket  $ticket
     * @return void
     */
    public function restored(Ticket $ticket)
    {
        //
    }

    /**
     * Handle the ticket "force deleted" event.
     *
     * @param  \App\Ticket  $ticket
     * @return void
     */
    public function forceDeleted(Ticket $ticket)
    {
        //
    }
}
