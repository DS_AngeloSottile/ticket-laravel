@extends('layouts.app')

@section('content')
    <div class="container col-md-8 col-md-offset-2 t-5">

        @include('components.message')

        <div class="card">
            <div class="card-header ">
                <h5 class="float-left">Tickets</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                @if ($tickets->isEmpty())
                    <p> There is no ticket.</p>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets as $ticket)
                            <tr>
                                <td>{{ $ticket->id }} </td>
                                <td> <a href="{{route('admin.tickets.show', $ticket)}}"> {{ $ticket->title }} </a> </td>

                                <td>{{ $ticket->status ? 'Pending' : 'Answered' }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection
