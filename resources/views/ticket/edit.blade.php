@extends('layouts.app')

@section('content')
    <div class="container col-md-8 col-md-offset-2">


        @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
        @endif
        @include('components.message')


        <div class="card mt-5">
            <div class="card-header ">
                <h5 class="float-left">Edit ticket</h5>
                <div class="clearfix"></div>
            </div>
            <div class="card-body mt-2">
                <form method="post" action="{{route('admin.tickets.update',$ticket)}}">
                    @method('patch')
                    @csrf

                    <fieldset>
                        <div class="form-group">
                            <label for="title" class="col-lg-2 control-label">Title</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{ $ticket->title }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content" class="col-lg-2 control-label">Content</label>
                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" id="content" name="content">{{ $ticket->content }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="status" {{ $ticket->status ? "" : "checked"}} > Close this ticket?
                            </label>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-primary">Update</button>
							</div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
