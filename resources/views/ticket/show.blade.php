@extends('layouts.app')

@section('content')
<div class="container col-md-8 col-md-offset-2 mt-5">
    <div class="card">
        <div class="card-header ">
            <h5 class="float-left">{{ $ticket->title }}</h5>
            <div class="clearfix"></div>
        </div>
        <div class="card-body">
            <p> <strong>Status</strong>: {{ $ticket->status ? 'Pending' : 'Answered' }}</p>
            <p> {{ $ticket->content }} </p>
            @foreach($ticket->comments as $comment)
                <div class="card mt-3">
                    <div class="card-body">
                        {{ $comment->content }}
                    </div>
                </div>
            @endforeach

            <a href="{{route('admin.tickets.edit', $ticket)}}" class="btn btn-info">Edit</a>

            <form  style="display: inline-block" method="POST" action="{{route('admin.tickets.destroy', $ticket )}}">
                @csrf
                @method('delete')

                <button type="submit"  class="btn btn-warning"> Delete </button>

            </form>

        </div>
    </div>


    <div class="card mt-3">
        <form method="post" action="{{route('admin.commentsCreated',$ticket)}}">
            @csrf

            @include('components.message')
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <fieldset>
                <legend class="ml-3">Reply</legend>


                <div class="form-group">
                    <div class="col-lg-12">
                        <textarea class="form-control" rows="3" id="content" name="content"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="reset" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-primary">Post</button>
                    </div>
                </div>
            </fieldset>

        </form>
    </div>

</div>
@endsection
