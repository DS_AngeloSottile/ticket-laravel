@if(session( 'success' ) )
                <div class="row">
                    <div class="col-xs-12"> 
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{session('success')}} 
                        </div>
                    </div>
                </div>
@endif

@if( session( 'error' ) )
    @php
        $creation_errors = session('error');
    @endphp
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                @isset( $creation_errors['error_message'] )
                    <strong>{{ $creation_errors['error_message'] }}</strong>
                @endisset
                @isset( $creation_errors['details'] )
                    <p>
                        {{ $creation_errors['details'] }}
                    </p>
                @endisset
            </div>
        </div>
    </div>
@endif

@if(session( 'warning' ) )
    <div class="row">
        <div class="col-xs-12"> 
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{session('warning')}} 
            </div>
        </div>
    </div>
@endif